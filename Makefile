.PHONY: install
install: stow

.PHONY: stow
stow:
	stow --target ${HOME}/.local/bin bin

.PHONY: clean
clean:
	stow --target ${HOME}/.local/bin --delete bin 2>/dev/null
